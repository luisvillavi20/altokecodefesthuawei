package com.txof.myapplication.restaurant;

import com.txof.myapplication.vm.ProductEntity;

public interface IRestaurant {
   void addProduct(ProductEntity productEntity);
}
