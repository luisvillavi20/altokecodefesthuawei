package com.txof.myapplication.restaurant;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.txof.myapplication.R;
import com.txof.myapplication.vm.ProductEntity;

import java.util.ArrayList;
import java.util.List;

public class RestaurantMenuAdapter extends RecyclerView.Adapter<RestaurantMenuAdapter.ViewHolder> {
   private List<ProductEntity> restaurantMenuList = new ArrayList<>();
   private Context context;
   private IRestaurant iRestaurant;

   public RestaurantMenuAdapter(Context context, IRestaurant iRestaurant) {
      this.context = context;
      this.iRestaurant = iRestaurant;
   }

   @NonNull
   @Override
   public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
      View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.restaurant_menu_item, parent, false);
      return new ViewHolder(view);
   }

   @Override
   public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
      ProductEntity productEntity = restaurantMenuList.get(position);
      holder.txtNombre.setText(productEntity.getcNombre());
      holder.txtDescri.setText(productEntity.getcDescri());
      holder.txtMonto.setText(String.format("S/%.2f", productEntity.getnMonto()));
      holder.setOnClickListener(productEntity);
   }

   @Override
   public int getItemCount() {
      return restaurantMenuList.size();
   }

   public void setData(List<ProductEntity> restaurantMenuList) {
      this.restaurantMenuList = restaurantMenuList;
      notifyDataSetChanged();
   }

   public class ViewHolder extends RecyclerView.ViewHolder {
      private TextView txtNombre;
      private TextView txtMonto;
      private TextView txtDescri;
      private Button btnAgregar;
      public ViewHolder(@NonNull View itemView) {
         super(itemView);
         mxBind();
      }

      private void mxBind() {
         txtNombre = itemView.findViewById(R.id.txtNombre);
         txtMonto = itemView.findViewById(R.id.txtMonto);
         txtDescri = itemView.findViewById(R.id.txtDescri);
         btnAgregar = itemView.findViewById(R.id.btnAgregar);
      }

      private void setOnClickListener(ProductEntity productEntity) {
         btnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               iRestaurant.addProduct(productEntity);
            }
         });
      }
   }
}
