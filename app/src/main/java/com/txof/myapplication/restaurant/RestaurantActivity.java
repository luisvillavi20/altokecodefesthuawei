package com.txof.myapplication.restaurant;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.txof.myapplication.R;
import com.txof.myapplication.cart.CartActivity;
import com.txof.myapplication.map.MapActivity;
import com.txof.myapplication.vm.Cart;
import com.txof.myapplication.vm.ProductEntity;
import com.txof.myapplication.vm.ProductService;
import com.txof.myapplication.vm.PromoEntity;
import com.txof.myapplication.vm.PromoService;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestaurantActivity extends AppCompatActivity implements IRestaurant {
   private static final String TAG = "RestaurantActivity";
   public static final String KEY_RESTAURANT_ID = "restaurant_id";
   private RestaurantMenuAdapter restaurantMenuAdapter;

   @BindView(R.id.recyclerViewMenu)
   RecyclerView recyclerViewMenu;
   @BindView(R.id.pedidoWrapper)
   View pedidoWrapper;
   @BindView(R.id.btnPedir)
   Button btnPedir;
   @BindView(R.id.btnCart)
   Button btnCart;
   private String cCodEmp;

   @Override
   protected void onCreate(@Nullable Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_resturant);
      getSupportActionBar().setDisplayHomeAsUpEnabled(true);
      cCodEmp = getIntent().getStringExtra(KEY_RESTAURANT_ID);
      ButterKnife.bind(this);
      mxSetup();
      mxLoad();
   }

   private void mxLoad() {
      // Retrofit
      Retrofit retrofit = new Retrofit.Builder()
              .baseUrl(this.getString(R.string.URL))
              .addConverterFactory(GsonConverterFactory.create())
              .build();
      // Productos del restaurante
      ProductService productService = retrofit.create(ProductService.class);
      Call<List<ProductEntity>> call = productService.getProducts(cCodEmp);
      call.enqueue(new Callback<List<ProductEntity>>() {
         @Override
         public void onResponse(Call<List<ProductEntity>> call, Response<List<ProductEntity>> response) {
            restaurantMenuAdapter.setData(response.body());
         }

         @Override
         public void onFailure(Call<List<ProductEntity>> call, Throwable t) {
            Log.e(TAG, t.getMessage());
         }
      });
      /*List<ProductEntity> restaurantMenuList = new ArrayList<>();
      restaurantMenuList.add(new ProductEntity("Ocopa", "Papas arequipeñas, con salsa de queso", 25f));
      restaurantMenuList.add(new ProductEntity("Cauche de queso", "Caldito hecho a base de queso, leche", 28f));
      restaurantMenuAdapter.setData(restaurantMenuList);*/
   }

   private void mxSetup() {
      if (Cart.getInstance().getCount() > 0) {
         btnPedir.setText(String.format("Pedir por S/%.2f", Cart.getInstance().getTotal()));
         btnCart.setText(String.format("Carta (%d)", Cart.getInstance().getCount()));
         pedidoWrapper.setVisibility(View.VISIBLE);
      }
      restaurantMenuAdapter = new RestaurantMenuAdapter(this, this);
      recyclerViewMenu.setAdapter(restaurantMenuAdapter);
      recyclerViewMenu.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
   }

   @Override
   public void addProduct(ProductEntity productEntity) {
      Cart.getInstance().addProduct(productEntity);
      btnPedir.setText(String.format("Pedir por S/%.2f", Cart.getInstance().getTotal()));
      btnCart.setText(String.format("Carta (%d)", Cart.getInstance().getCount()));
      pedidoWrapper.setVisibility(View.VISIBLE);
   }

   @OnClick(R.id.btnPedir)
   public void onClick(View v) {
      Intent intent = new Intent(this, MapActivity.class);
      startActivity(intent);
   }

   @OnClick(R.id.btnCart)
   public void showCart(View view) {
      Intent intent = new Intent(this, CartActivity.class);
      startActivity(intent);
   }
}
