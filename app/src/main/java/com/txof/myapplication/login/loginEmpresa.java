package com.txof.myapplication.login;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Toast;
import android.view.View;
import android.widget.Button;
import android.content.Intent;

import com.txof.myapplication.R;

public class loginEmpresa extends AppCompatActivity {

    Button btn_ir_crearCuenta2,iniciar_sesion, volver;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_empresa);

        btn_ir_crearCuenta2=findViewById(R.id.btn_ir_crearCuenta2);
        iniciar_sesion=findViewById(R.id.iniciar_sesion);
        volver=findViewById(R.id.volver);

        btn_ir_crearCuenta2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(loginEmpresa.this,SingUpEmpresa.class));
                finish();
            }
        });

        iniciar_sesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Toast.makeText(loginEmpresa.this, "Bienvendio Usuario:", Toast.LENGTH_SHORT).show();

            }
        });

        volver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(loginEmpresa.this, AccountSelectorActivity.class));
                finish();
            }
        });
    }
}