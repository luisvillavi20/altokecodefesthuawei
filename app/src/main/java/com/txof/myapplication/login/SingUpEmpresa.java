package com.txof.myapplication.login;

import androidx.appcompat.app.AppCompatActivity;
import com.txof.myapplication.R;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import android.os.Bundle;

public class SingUpEmpresa extends AppCompatActivity {

    Button btn_ir_inicioSesion,crear_cuenta;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sing_up_empresa);

        btn_ir_inicioSesion=findViewById(R.id.btn_ir_inicioSesion);
        crear_cuenta=findViewById(R.id.crear_cuenta2);

        btn_ir_inicioSesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SingUpEmpresa.this,loginEmpresa.class));
                finish();
            }
        });

        crear_cuenta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(SingUpEmpresa.this, "Registrar Usuario", Toast.LENGTH_SHORT).show();
                //Aquí codigo
            }
        });
    }
}