package com.txof.myapplication.login;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
//import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.txof.myapplication.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class AccountSelectorActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
    }
    @OnClick(R.id.ir_login)
    void loginCliente(View view) {
        startActivity(new Intent(AccountSelectorActivity.this, LoginActivity.class));
        finish();
    }
    @OnClick(R.id.ir_login2)
    void loginEmpresa(View view) {
        startActivity(new Intent(AccountSelectorActivity.this, loginEmpresa.class));
        finish();
    }
}