package com.txof.myapplication.login;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.txof.myapplication.R;
import com.txof.myapplication.ui.MainActivity;
import com.txof.myapplication.util.SharedUtils;
import com.txof.myapplication.vm.UsuarioEntity;
import com.txof.myapplication.vm.UsuarioService;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SignUp extends AppCompatActivity {

    @BindView(R.id.et_nombre)
    EditText et_nombre;
    @BindView(R.id.et_correo)
    EditText et_correo;
    @BindView(R.id.et_telefono)
    EditText et_telefono;
    @BindView(R.id.et_contrasena)
    EditText et_contrasena;
    @BindView(R.id.mainView)
    View mainView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);
    }
    @OnClick(R.id.btn_ir_inicioSesion)
    void inicioSesion(View view) {
        startActivity(new Intent(SignUp.this, LoginActivity.class));
        finish();
    }
    @OnClick(R.id.crear_cuenta)
    void crearCuenta(View view) {
        // Retrofit
        Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(this.getString(R.string.URL))
            .addConverterFactory(GsonConverterFactory.create())
            .build();
        UsuarioService usuarioService = retrofit.create(UsuarioService.class);
        Call<UsuarioEntity> call = usuarioService.createUser(
                et_nombre.getText().toString(),
                et_telefono.getText().toString(),
                et_correo.getText().toString(),
                et_contrasena.getText().toString());
        call.enqueue(new Callback<UsuarioEntity>() {
            @Override
            public void onResponse(Call<UsuarioEntity> call, Response<UsuarioEntity> response) {
                UsuarioEntity usuarioEntity = response.body();
                UsuarioEntity.getInstance().setData(usuarioEntity);

                SharedUtils.setSession(SignUp.this, true);
                SharedUtils.setUsuario(SignUp.this, usuarioEntity.getcCodUsu());
                SharedUtils.setToken(SignUp.this, usuarioEntity.getcToken());

                Intent intent = new Intent(SignUp.this, MainActivity.class);
                startActivity(intent);
                finish();
            }

            @Override
            public void onFailure(Call<UsuarioEntity> call, Throwable t) {
                Snackbar.make(mainView, t.getMessage(), Snackbar.LENGTH_SHORT).show();
            }
        });
    }
}