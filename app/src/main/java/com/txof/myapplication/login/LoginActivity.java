package com.txof.myapplication.login;

import androidx.appcompat.app.AppCompatActivity;
//import android.support.v7.app.AppCompatActivity;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.content.Intent;
import android.widget.EditText;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.GsonBuilder;
import com.txof.myapplication.R;
import com.txof.myapplication.ui.MainActivity;
import com.txof.myapplication.util.SharedUtils;
import com.txof.myapplication.vm.UsuarioEntity;
import com.txof.myapplication.vm.UsuarioService;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginActivity extends AppCompatActivity {
    private static final String TAG = "LOGINACTIVITY";
    @BindView(R.id.et_correo)
    EditText et_correo;
    @BindView(R.id.et_contrasena)
    EditText et_contrasena;
    @BindView(R.id.mainView)
    View mainView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_cliente);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btn_ir_crearCuenta)
    void crearCuenta(View view) {
        startActivity(new Intent(LoginActivity.this, SignUp.class));
        finish();
    }
    @OnClick(R.id.iniciar_sesion)
    void iniciarSesion(View view) {
        // Retrofit
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(this.getString(R.string.URL))
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        UsuarioService usuarioService = retrofit.create(UsuarioService.class);
        Call<UsuarioEntity> call = usuarioService.iniciarSesion(
                et_correo.getText().toString(),
                et_contrasena.getText().toString()
        );
        call.enqueue(new Callback<UsuarioEntity>() {
            @Override
            public void onResponse(Call<UsuarioEntity> call, Response<UsuarioEntity> response) {
                UsuarioEntity usuarioEntity = response.body();
                if (usuarioEntity.getERROR() != null) {
                    Snackbar.make(mainView, usuarioEntity.getERROR(), Snackbar.LENGTH_SHORT)
                            .setTextColor(Color.WHITE)
                            .show();
                    return;
                }
                UsuarioEntity.getInstance().setData(usuarioEntity);

                SharedUtils.setSession(LoginActivity.this, true);
                SharedUtils.setUsuario(LoginActivity.this, usuarioEntity.getcCodUsu());
                SharedUtils.setToken(LoginActivity.this, usuarioEntity.getcToken());

                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }

            @Override
            public void onFailure(Call<UsuarioEntity> call, Throwable t) {
                Snackbar.make(mainView, t.getMessage(), Snackbar.LENGTH_SHORT)
                        .setTextColor(Color.WHITE)
                        .show();
            }
        });
    }
    @OnClick(R.id.volver)
    void volver(View view) {
        startActivity(new Intent(LoginActivity.this, AccountSelectorActivity.class));
        finish();
    }
}