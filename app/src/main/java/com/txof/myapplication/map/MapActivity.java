package com.txof.myapplication.map;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;

import com.huawei.hms.maps.CameraUpdate;
import com.huawei.hms.maps.HuaweiMap;
import com.huawei.hms.maps.MapView;
import com.huawei.hms.maps.OnMapReadyCallback;
import com.huawei.hms.maps.model.CameraPosition;
import com.huawei.hms.maps.model.LatLng;
import com.huawei.hms.maps.model.Marker;
import com.huawei.hms.maps.model.MarkerOptions;
import com.huawei.hms.maps.model.internal.IMarkerDelegate;
import com.huawei.hms.site.api.SearchResultListener;
import com.huawei.hms.site.api.SearchService;
import com.huawei.hms.site.api.SearchServiceFactory;
import com.txof.myapplication.R;
import com.txof.myapplication.checkout.CheckoutActivity;
import com.txof.myapplication.vm.Cart;

import java.io.IOException;
import java.util.Iterator;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback, HuaweiMap.OnMapLongClickListener {
   private static final String TAG = "MapActivity";
   private static final String MAPVIEW_BUNDLE_KEY = "MapViewBundleKey";
   private static final int REQUEST_CODE = 4978;
   private HuaweiMap hMap;
   private MapView mMapView;
   private Marker currentMarker;
   private CardView deliveryInfo;
   private TextView txtUbicacion;
   @BindView(R.id.btnPedir)
   Button btnPedir;

   private static final String[] RUNTIME_PERMISSIONS = {
           Manifest.permission.READ_EXTERNAL_STORAGE,
           Manifest.permission.WRITE_EXTERNAL_STORAGE,
           Manifest.permission.ACCESS_COARSE_LOCATION,
           Manifest.permission.ACCESS_FINE_LOCATION,
           Manifest.permission.INTERNET
   };

   @Override
   protected void onCreate(@Nullable Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_location);
      ButterKnife.bind(this);
      getSupportActionBar().setDisplayHomeAsUpEnabled(true);

      if (!hasPermissions(this, RUNTIME_PERMISSIONS)) {
         ActivityCompat.requestPermissions(this, RUNTIME_PERMISSIONS, REQUEST_CODE);
      }

      mMapView = findViewById(R.id.mapView);
      Bundle mapViewBundle = null;
      if (savedInstanceState != null) {
         mapViewBundle = savedInstanceState.getBundle(MAPVIEW_BUNDLE_KEY);
      }
      mMapView.onCreate(mapViewBundle);
      //get map instance
      mMapView.getMapAsync(this);
      mxBind();
   }

   private void mxBind() {
      txtUbicacion = findViewById(R.id.txtUbicacion);
      deliveryInfo = findViewById(R.id.cardView_DeliveryInfo);
      btnPedir.setText(String.format("Pedir por S/%.2f", Cart.getInstance().getTotal()));
   }

   @Override
   public void onMapReady(HuaweiMap huaweiMap) {
      Log.e(TAG, "onMapReady");
      hMap = huaweiMap;
      hMap.setMyLocationEnabled(true);
      hMap.getUiSettings().setMyLocationButtonEnabled(true);
      hMap.setOnMapLongClickListener(this);
   }

   private static boolean hasPermissions(Context context, String... permissions) {
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && permissions != null) {
         for (String permission : permissions) {
            if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
               return false;
            }
         }
      }
      return true;
   }

   @Override
   protected void onStart() {
      super.onStart();
      mMapView.onStart();
   }

   @Override
   protected void onStop() {
      super.onStop();
      mMapView.onStop();
   }

   @Override
   protected void onPause() {
      mMapView.onPause();
      super.onPause();
   }

   @Override
   protected void onResume() {
      super.onResume();
      mMapView.onResume();
   }

   @Override
   protected void onDestroy() {
      mMapView.onDestroy();
      super.onDestroy();
   }

   @Override
   public void onMapLongClick(LatLng latLng) {
      MarkerOptions markerOptions = new MarkerOptions().position(latLng);
      Marker tmpMarker = hMap.addMarker(markerOptions);
      if (this.currentMarker != null) {
         this.currentMarker.remove();
      }
      currentMarker = tmpMarker;
      txtUbicacion.setText("");
      deliveryInfo.setVisibility(View.VISIBLE);
      try {
         Iterator<Address> addressIterator = new Geocoder(this, Locale.getDefault()).getFromLocation(latLng.latitude, latLng.longitude, 1).iterator();
         while (addressIterator.hasNext()) {
            Address address = addressIterator.next();
            // address.getAdminArea() - Arequipa
            // address.getSubAdminArea() - Arequipa
            // address.getSubLocality() - Umacollo
            // address.getSubLocality() - 202
            txtUbicacion.setText(String.format("%s, %s %s",
                    address.getSubAdminArea(),
                    address.getThoroughfare(),
                    address.getSubThoroughfare()));
         }
      } catch (IOException e) {
         e.printStackTrace();
      }
   }

   @OnClick(R.id.btnPedir)
   void onClick(View view) {
      Intent intent = new Intent(this, CheckoutActivity.class);
      intent.putExtra(CheckoutActivity.ADDRESS_KEY, txtUbicacion.getText().toString());
      startActivity(intent);
   }
}
