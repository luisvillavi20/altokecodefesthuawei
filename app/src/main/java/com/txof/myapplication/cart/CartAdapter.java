package com.txof.myapplication.cart;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.txof.myapplication.R;
import com.txof.myapplication.vm.ProductEntity;

import java.util.ArrayList;
import java.util.List;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.ViewHolder> {
   private List<ProductEntity> productEntityList;
   private ICart iCart;

   public CartAdapter(List<ProductEntity> productEntityList, ICart iCart) {
      this.productEntityList = productEntityList;
      this.iCart = iCart;
   }

   @NonNull
   @Override
   public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
      View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cart_item, parent, false);
      return new ViewHolder(view);
   }

   @Override
   public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
      ProductEntity productEntity = productEntityList.get(position);
      holder.txtNombre.setText(productEntity.getcDescri());
      holder.txtMonto.setText(String.valueOf(productEntity.getnMonto()));
      holder.setOnClickListener(productEntity);
   }

   @Override
   public int getItemCount() {
      return this.productEntityList.size();
   }

   public void setProducts(List<ProductEntity> products) {
      this.productEntityList = products;
      notifyDataSetChanged();
   }

   public class ViewHolder extends RecyclerView.ViewHolder {
      private Button btnQuitarItem;
      private TextView txtNombre;
      private TextView txtMonto;
      public ViewHolder(@NonNull View itemView) {
         super(itemView);
         btnQuitarItem = itemView.findViewById(R.id.btnQuitarItem);
         txtNombre = itemView.findViewById(R.id.txtNombre);
         txtMonto = itemView.findViewById(R.id.txtMonto);
      }
      public void setOnClickListener(ProductEntity productEntity) {
         btnQuitarItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               iCart.removeItem(productEntity);
            }
         });
      }
   }
}
