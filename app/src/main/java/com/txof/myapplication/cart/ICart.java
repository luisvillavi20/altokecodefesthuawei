package com.txof.myapplication.cart;

import com.txof.myapplication.vm.ProductEntity;

public interface ICart {
   void removeItem(ProductEntity productEntity);
}
