package com.txof.myapplication.cart;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.txof.myapplication.R;
import com.txof.myapplication.vm.Cart;
import com.txof.myapplication.vm.ProductEntity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CartActivity extends AppCompatActivity implements ICart {
   @BindView(R.id.recyclerViewCart)
   RecyclerView recyclerViewCart;
   private CartAdapter cartAdapter;

   @Override
   protected void onCreate(@Nullable Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_cart);
      ButterKnife.bind(this);
      setup();
   }

   private void setup() {
      cartAdapter = new CartAdapter(Cart.getInstance().getProducts(), this);
      recyclerViewCart.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
      recyclerViewCart.setAdapter(cartAdapter);
   }

   @Override
   public void removeItem(ProductEntity productEntity) {
      Cart.getInstance().removeProduct(productEntity);
      if (Cart.getInstance().getCount() == 0) {
         finish();
      }
      cartAdapter.setProducts(Cart.getInstance().getProducts());
   }
}
