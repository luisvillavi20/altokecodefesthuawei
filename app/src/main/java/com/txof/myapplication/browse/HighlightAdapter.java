package com.txof.myapplication.browse;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.txof.myapplication.R;
import com.txof.myapplication.vm.HighlightEntity;
import com.txof.myapplication.vm.PromoEntity;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class HighlightAdapter extends RecyclerView.Adapter<HighlightAdapter.ViewHolder> {
   private static final String TAG = "PromoAdapter";
   protected List<HighlightEntity> promoList = new ArrayList();
   protected Context context;
   protected IBrowse iBrowse;

   public HighlightAdapter(Context context, IBrowse iBrowse) {
      this.context = context;
      this.iBrowse = iBrowse;
   }

   @NonNull
   @Override
   public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
      View loView = LayoutInflater.from(parent.getContext()).inflate(R.layout.promo_item, parent,false);
      return new ViewHolder(loView);
   }

   @Override
   public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
      HighlightEntity loTmp = this.promoList.get(position);
      holder.txtPromoTitle.setText(loTmp.getcNombre());
      Glide.with(context)
              .load(loTmp.getcImagen())
              .diskCacheStrategy(DiskCacheStrategy.ALL)
              .into(holder.img_promo);
      holder.setOnClickListener(loTmp.getcId());
   }

   @Override
   public int getItemCount() {
      return promoList.size();
   }

   public void setData(List<HighlightEntity> p_aHighlight) {
      promoList = p_aHighlight;
      notifyDataSetChanged();
   }

   public class ViewHolder extends RecyclerView.ViewHolder{
      private ImageView img_promo;
      private TextView txtPromoTitle;
      private RelativeLayout promoWrapper;
      public ViewHolder(@NonNull View itemView) {
         super(itemView);
         mxBind();
      }

      private void mxBind() {
         txtPromoTitle = itemView.findViewById(R.id.txtPromoTitle);
         promoWrapper = itemView.findViewById(R.id.promoWrapper);
         img_promo = itemView.findViewById(R.id.img_promo);
      }

      private void setOnClickListener(String id) {
         itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               iBrowse.onRestautantClick(id);
            }
         });
      }
   }
}