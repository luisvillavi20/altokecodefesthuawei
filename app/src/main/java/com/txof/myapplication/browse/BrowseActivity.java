package com.txof.myapplication.browse;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SearchView;

import com.ethanhua.skeleton.RecyclerViewSkeletonScreen;
import com.ethanhua.skeleton.Skeleton;
import com.google.android.material.snackbar.Snackbar;
import com.txof.myapplication.R;
import com.txof.myapplication.cart.CartActivity;
import com.txof.myapplication.map.MapActivity;
import com.txof.myapplication.restaurant.RestaurantActivity;
import com.txof.myapplication.search.SearchActivity;
import com.txof.myapplication.vm.Cart;
import com.txof.myapplication.vm.HighlightEntity;
import com.txof.myapplication.vm.HighlightService;
import com.txof.myapplication.vm.PromoEntity;
import com.txof.myapplication.vm.PromoService;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BrowseActivity extends Fragment implements View.OnClickListener, IBrowse {
    private static final String TAG = "BrowseActivity";
    private PromoAdapter promoAdapter;
    private HighlightAdapter highlightAdapter;
    private RecyclerViewSkeletonScreen promoSkeleton;
    private RecyclerViewSkeletonScreen highlightSkeleton;

    @BindView(R.id.recyclerViewPromo)
    RecyclerView recyclerViewPromo;
    @BindView(R.id.recyclerViewHighlight)
    RecyclerView recyclerViewHighlight;
    @BindView(R.id.pedidoWrapper)
    View pedidoWrapper;
    @BindView(R.id.btnPedir)
    Button btnPedir;
    @BindView(R.id.btnCart)
    Button btnCart;
    @BindView(R.id.searchView)
    SearchView searchView;
    @BindView(R.id.main)
    View mainView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.activity_browse, container, false);
        ButterKnife.bind(this, view);
        return view;
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mxBind();
        mxSetup();
        mxLoad();
    }

    public static BrowseActivity newInstance() {
        BrowseActivity fragment = new BrowseActivity();
        return fragment;
    }

    private void mxLoad() {
        // Retrofit
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getContext().getString(R.string.URL))
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        // Promociones
        PromoService promoService = retrofit.create(PromoService.class);
        Call<List<PromoEntity>> call = promoService.getPromo();
        call.enqueue(new Callback<List<PromoEntity>>() {
            @Override
            public void onResponse(Call<List<PromoEntity>> call, Response<List<PromoEntity>> response) {
                promoAdapter.setData(response.body());
                promoSkeleton.hide();
            }

            @Override
            public void onFailure(Call<List<PromoEntity>> call, Throwable t) {
                Snackbar.make(mainView, t.getMessage(), Snackbar.LENGTH_SHORT).show();
                Log.e(TAG, t.getMessage());
            }
        });
        // Destacados
        HighlightService highlightService = retrofit.create(HighlightService.class);
        Call<List<HighlightEntity>> callHighlight = highlightService.getHighlight();
        callHighlight.enqueue(new Callback<List<HighlightEntity>>() {
            @Override
            public void onResponse(Call<List<HighlightEntity>> call, Response<List<HighlightEntity>> response) {
                highlightAdapter.setData(response.body());
                highlightSkeleton.hide();
            }

            @Override
            public void onFailure(Call<List<HighlightEntity>> call, Throwable t) {
                Snackbar.make(mainView, t.getMessage(), Snackbar.LENGTH_SHORT).show();
                Log.e(TAG, t.getMessage());
            }
        });
    }

    private void mxSetup() {
        if (Cart.getInstance().getCount() > 0) {
            btnPedir.setText(String.format("Pedir por S/%.2f", Cart.getInstance().getTotal()));
            btnCart.setText(String.format("Carta (%d)", Cart.getInstance().getCount()));
            pedidoWrapper.setVisibility(View.VISIBLE);
        }
        promoAdapter = new PromoAdapter(getContext(), this);
        recyclerViewPromo.setAdapter(promoAdapter);
        recyclerViewPromo.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false));
        promoSkeleton = Skeleton.bind(recyclerViewPromo)
                .adapter(promoAdapter)
                .load(R.layout.promo_item_skeleton)
                .count(5)
                .show();
        // Destacados
        highlightAdapter = new HighlightAdapter(getContext(), this);
        recyclerViewHighlight.setAdapter(highlightAdapter);
        recyclerViewHighlight.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false));
        highlightSkeleton = Skeleton.bind(recyclerViewHighlight)
                .adapter(highlightAdapter)
                .load(R.layout.promo_item_skeleton)
                .count(5)
                .show();
    }

    private void mxBind() {
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (query.isEmpty()) {
                    Snackbar.make(mainView, "Ingrese texto de busqueda", Snackbar.LENGTH_SHORT).show();
                    return false;
                }
                Intent intent = new Intent(getContext(), SearchActivity.class);
                intent.putExtra(SearchActivity.KEY_SEARCH_TEXT, query);
                startActivity(intent);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
    }

    @OnClick(R.id.btnPedir)
    public void onClick(View v) {
        Intent intent = new Intent(getContext(), MapActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.btnCart)
    public void showCart(View view) {
        Intent intent = new Intent(getContext(), CartActivity.class);
        startActivity(intent);
    }

    @Override
    public void onRestautantClick(String id) {
        Intent intent = new Intent(getContext(), RestaurantActivity.class);
        intent.putExtra(RestaurantActivity.KEY_RESTAURANT_ID, id);
        startActivity(intent);
    }
}
