package com.txof.myapplication.vm;

import java.io.Serializable;

public class UsuarioEntity {
   private String cCodUsu;
   private String cNombre;
   private String cEmail;
   private String cNroCel;
   private String cToken;
   private String ERROR;

   public String getcCodUsu() {
      return cCodUsu;
   }

   public void setcCodUsu(String cCodUsu) {
      this.cCodUsu = cCodUsu;
   }

   public String getcNombre() {
      return cNombre;
   }

   public void setcNombre(String cNombre) {
      this.cNombre = cNombre;
   }

   public String getcEmail() {
      return cEmail;
   }

   public void setcEmail(String cEmail) {
      this.cEmail = cEmail;
   }

   public String getcNroCel() {
      return cNroCel;
   }

   public void setcNroCel(String cNroCel) {
      this.cNroCel = cNroCel;
   }

   public String getcToken() {
      return cToken;
   }

   public void setcToken(String cToken) {
      this.cToken = cToken;
   }

   public String getERROR() {
      return ERROR;
   }

   public void setERROR(String ERROR) {
      this.ERROR = ERROR;
   }

   public void setData(UsuarioEntity usuarioEntity) {
      this.setcCodUsu(usuarioEntity.cCodUsu);
      this.setcEmail(usuarioEntity.cEmail);
      this.setcNombre(usuarioEntity.cNombre);
      this.setcNroCel(usuarioEntity.cNroCel);
      this.setcToken(usuarioEntity.cToken);
   }

   private final static class SINGLETON_HOLDER {
      public static final UsuarioEntity INSTANCE = new UsuarioEntity();
   }

   public static UsuarioEntity getInstance() {
      return SINGLETON_HOLDER.INSTANCE;
   }
}
