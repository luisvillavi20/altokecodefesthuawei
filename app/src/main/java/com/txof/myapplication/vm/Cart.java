package com.txof.myapplication.vm;

import java.util.ArrayList;
import java.util.List;

public class Cart {
   private List<ProductEntity> productEntityList = new ArrayList<>();

   public void addProduct(ProductEntity productEntity) {
      this.productEntityList.add(productEntity);
   }

   public double getTotal() {
      double lnMonto = 0f;
      for (ProductEntity productEntity : productEntityList) {
         lnMonto += productEntity.getnMonto();
      }
      return lnMonto;
   }

   public int getCount() {
      return this.productEntityList.size();
   }

   public static final Cart getInstance() {
      return CartSingleton.INSTANCE;
   }

   public List<ProductEntity> getProducts() {
      return this.productEntityList;
   }

   public void removeProduct(ProductEntity productEntity) {
      this.productEntityList.remove(productEntity);
   }

   public void removeAll() {
      this.productEntityList.clear();
   }

   private static final class CartSingleton {
      public static final Cart INSTANCE = new Cart();
   }
}
