package com.txof.myapplication.vm;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface UsuarioService {
   String API_ROUTE = "User";

   @POST(API_ROUTE)
   Call<UsuarioEntity> createUser(@Query("cNombre") String cNombre, @Query("cNroCel") String cNroCel, @Query("cEmail") String cEmail, @Query("cClave") String cClave);

   @POST(API_ROUTE + "/Iniciar")
   Call<UsuarioEntity> iniciarSesion(@Query("cEmail") String cEmail, @Query("cClave") String cClave);
}
