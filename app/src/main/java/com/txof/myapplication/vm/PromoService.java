package com.txof.myapplication.vm;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface PromoService {
   String API_ROUTE = "Promo";

   @GET(API_ROUTE)
   Call<List<PromoEntity>> getPromo();
}
