package com.txof.myapplication.vm;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface SearchService {
   String API_ROUTE = "Search";

   @GET(API_ROUTE)
   Call<List<SearchEntity>> getResult();
}
