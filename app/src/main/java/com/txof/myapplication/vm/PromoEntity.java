package com.txof.myapplication.vm;

public class PromoEntity {
    private String cNombre;
    private String cImagen;
    private String cId;

    public PromoEntity(String cNombre, String cImagen, String cId) {
        this.cNombre = cNombre;
        this.cImagen = cImagen;
        this.cId = cId;
    }

    public String getcNombre() {
        return cNombre;
    }

    public void setcNombre(String cNombre) {
        this.cNombre = cNombre;
    }

    public String getcImagen() {
        return cImagen;
    }

    public void setcImagen(String cImagen) {
        this.cImagen = cImagen;
    }

    public String getcId() {
        return cId;
    }

    public void setcId(String cId) {
        this.cId = cId;
    }
}
