package com.txof.myapplication.vm;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ProductService {
   String API_ROUTE = "Restaurant";

   @GET(API_ROUTE)
   Call<List<ProductEntity>> getProducts(@Query("cCodEmp") String cCodEmp);
}
