package com.txof.myapplication.vm;

import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface OrderService {
   String API_ROUTE = "Order";

   @POST(API_ROUTE)
   Call<OrderEntity> order(@Query("cCodUsu") String cCodUsu, @Query("cToken") String cToken, @Query("cNotas") String cNotas, @Query("products") String products);
}
