package com.txof.myapplication.vm;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface HighlightService {
   String API_ROUTE = "Highlight";

   @GET(API_ROUTE)
   Call<List<HighlightEntity>> getHighlight();
}
