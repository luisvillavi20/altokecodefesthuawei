package com.txof.myapplication.vm;

public class ProductEntity {
   private String cIdProd;
   private String cNombre;
   private String cDescri;
   private double nMonto;

   public ProductEntity(String cNombre, String cDescri, double nMonto) {
      this.cNombre = cNombre;
      this.cDescri = cDescri;
      this.nMonto = nMonto;
   }

   public String getcNombre() {
      return cNombre;
   }

   public void setcNombre(String cNombre) {
      this.cNombre = cNombre;
   }

   public String getcDescri() {
      return cDescri;
   }

   public void setcDescri(String cDescri) {
      this.cDescri = cDescri;
   }

   public double getnMonto() {
      return nMonto;
   }

   public void setnMonto(double nMonto) {
      this.nMonto = nMonto;
   }

   public String getcIdProd() {
      return cIdProd;
   }

   public void setcIdProd(String cIdProd) {
      this.cIdProd = cIdProd;
   }
}
