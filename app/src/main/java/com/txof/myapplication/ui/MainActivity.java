package com.txof.myapplication.ui;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.material.navigation.NavigationView;
import com.txof.myapplication.R;
import com.txof.myapplication.browse.BrowseActivity;
import com.txof.myapplication.ui.activities.LauncherActivity;
import com.txof.myapplication.ui.menu.NavMenuAdapter;
import com.txof.myapplication.ui.menu.NavMenuModel;
import com.txof.myapplication.ui.menu.SubTitle;
import com.txof.myapplication.ui.menu.TitleMenu;
import com.txof.myapplication.ui.signup.EnterpriseFragment;
import com.txof.myapplication.util.SharedUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements NavMenuAdapter.MenuItemClickListener {

    private ArrayList<NavMenuModel> menu = new ArrayList<>();
    private PackageInfo packageInfo;
    NavMenuAdapter adapter;
    RecyclerView navMenuDrawer;

    @BindView(R.id.nav_view)
    NavigationView navigationView;
    DrawerLayout drawerLayout;
    @BindView(R.id.lblInfoUser)
    TextView lblTitle;
    @BindView(R.id.lblInfoVersion)
    TextView lblSubtitle;
    @BindView(R.id.imgUser)
    ImageView imgUser;
    @BindView(R.id.loaderMain)
    SwipeRefreshLayout loaderMain;
    @BindView(R.id.toolbar)
    Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        setSupportActionBar(toolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.setDrawerListener(toggle);
        toggle.syncState();
        getSupportActionBar().setTitle(getString(R.string.app_name));
        setHeader();
        setNavigationDrawerMenu();
    }

    @Override
    public void onMenuItemClick(String itemString) {
        for (int i = 0; i < menu.size(); i++) {
            if (itemString.equals(menu.get(i).menuTitle)) {
                if (menu.get(i).menuTitle.equals("Salir")) {
                    displaySignOutDialog();
                } else {
                    if (menu.get(i).fragment != null) {
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.container_body, menu.get(i).fragment)
                                .commitAllowingStateLoss();
                        setTitle(SharedUtils.toTitleCase(menu.get(i).menuTitle));
                    } else {
                        if (menu.get(i).subMenu.size() > 0) {
                            getSupportFragmentManager().beginTransaction()
                                    .replace(R.id.container_body, menu.get(i).subMenu.get(0).fragment)
                                    .commitAllowingStateLoss();
                            setTitle(SharedUtils.toTitleCase(menu.get(i).subMenu.get(0).subMenuTitle));
                        }
                    }
                }
                break;
            } else {
                for (int j = 0; j < menu.get(i).subMenu.size(); j++) {
                    if (itemString.equals(menu.get(i).subMenu.get(j).subMenuTitle)) {
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.container_body, menu.get(i).subMenu.get(j).fragment)
                                .commitAllowingStateLoss();
                        setTitle(SharedUtils.toTitleCase(menu.get(i).subMenu.get(j).subMenuTitle));
                    }
                }
            }
        }
        if (drawerLayout != null) {
            if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                drawerLayout.closeDrawer(GravityCompat.START);
            } else {
                //drawerLayout.openDrawer(GravityCompat.START);
            }
        }
    }

    private void setNavigationDrawerMenu() {
        NavMenuAdapter adapter;
        adapter = new NavMenuAdapter(this, getMenuList(), this);
        navMenuDrawer = (RecyclerView) findViewById(R.id.main_nav_menu_recyclerview);
        navMenuDrawer.setAdapter(adapter);
        navMenuDrawer.setLayoutManager(new LinearLayoutManager(this));
        navMenuDrawer.setAdapter(adapter);
        adapter.selectedItemParent = menu.get(0).menuTitle;
        onMenuItemClick(adapter.selectedItemParent);
        adapter.notifyDataSetChanged();
    }


    private void setHeader() {
        try {
            String urlPhoto = "";
            /*GlideApp.with(this)
                    .load(urlPhoto)
                    .placeholder(R.drawable.ic_img_circle)
                    .error(R.drawable.ic_img_circle)
                    .circleCrop()
                    .into(imgUser);*/

            lblTitle.setText(SharedUtils.getUsuario(this));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean ContainMenu(String menuName) {
        if (menu != null) {
            for (int i = 0; i < menu.size(); i++) {
                if (menu.get(i).menuTitle.equalsIgnoreCase(menuName))
                    return true;
            }
        }
        return false;
    }

    private List<TitleMenu> getMenuList() {
        List<TitleMenu> list = new ArrayList<>();
        this.menu = getMenu();
        if (menu != null) {
            for (int i = 0; i < menu.size(); i++) {
                ArrayList<SubTitle> subMenu = new ArrayList<>();
                if (menu.get(i).subMenu.size() > 0) {
                    for (int j = 0; j < subMenu.size(); j++) {
                        subMenu.add(new SubTitle(menu.get(i).subMenu.get(j).subMenuTitle, menu.get(i).subMenu.get(j).count));
                    }
                }
                list.add(new TitleMenu(menu.get(i).menuTitle, subMenu, menu.get(i).menuIconDrawable));
            }
        }
        return list;
    }

    private ArrayList<NavMenuModel> getMenu() {
        try {
            menu.add(new NavMenuModel("Inicio", R.drawable.ic_home, new BrowseActivity()));
            menu.add(new NavMenuModel("Quiero ser restaurante", R.drawable.ic_ajustes, new EnterpriseFragment()));
            menu.add(new NavMenuModel("Salir", R.drawable.ic_fin_session, new EnterpriseFragment()));
            return menu;
        } catch (Exception ex) {
            return null;
        }
    }

    public void navigateTo(Fragment fragment, String title, int index) {
        try {
            if (index > 0) {
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container_body, menu.get(index).fragment)
                        .commit();
                setTitle(SharedUtils.toTitleCase(menu.get(index).menuTitle));
            } else {
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container_body, fragment)
                        .commit();
                setTitle(SharedUtils.toTitleCase(title));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void displaySignOutDialog() {
        new MaterialDialog.Builder(this)
                .title("Salir")
                .content("¿Desea cerrar la aplicacion?")
                .cancelable(false)
                .positiveText("SI")
                .onPositive((dialog, which) -> {
                    SharedUtils.setSession(this, false);
                    finish();
                })
                .negativeText("NO")
                .show();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            drawerLayout.openDrawer(GravityCompat.START);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
