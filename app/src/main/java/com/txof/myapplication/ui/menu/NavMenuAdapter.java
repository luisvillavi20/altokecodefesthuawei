package com.txof.myapplication.ui.menu;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.core.content.ContextCompat;

import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.txof.myapplication.R;

import java.util.ArrayList;
import java.util.List;


public class NavMenuAdapter extends ExpandableRecyclerViewAdapter<TitleViewHolder, SubTitleViewHolder> {
    private Context context;
    private MenuItemClickListener mListener;
    public String selectedItemParent = "";
    public String selectedItemChild = "";
    public ArrayList<String> isExpandList = new ArrayList<>();

    public NavMenuAdapter(Context context, List<? extends ExpandableGroup> groups, Activity activity) {
        super(groups);
        this.context = context;
        this.mListener = (MenuItemClickListener) activity;
    }

    @Override
    public TitleViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.nav_menu_item, parent, false);
        TitleViewHolder holder = new TitleViewHolder(view, this);
        holder.setIsRecyclable(false);
        return holder;
    }

    @Override
    public SubTitleViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.nav_submenu_item, parent, false);
        SubTitleViewHolder holder = new SubTitleViewHolder(view);
        holder.setIsRecyclable(false);
        return holder;
    }

    @Override
    public void onBindChildViewHolder(final SubTitleViewHolder holder, final int flatPosition,
                                      ExpandableGroup group, final int childIndex) {
        final TitleMenu menu = ((TitleMenu) group);
        final SubTitle subTitle = menu.getItems().get(childIndex);

        if (selectedItemChild.equals(subTitle.getName()))
            holder.itemView.setBackgroundColor(ContextCompat.getColor(context, R.color.navSubmenuSelected));
        else
            holder.itemView.setBackgroundColor(Color.WHITE);


        holder.setSubTitletName(subTitle.getName());
        if (subTitle.getCount() != 0) {
            holder.setCount(subTitle.getCount());
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedItemParent = menu.getTitle();
                selectedItemChild = subTitle.getName();
                mListener.onMenuItemClick(subTitle.getName());
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public void onBindGroupViewHolder(final TitleViewHolder holder, final int flatPosition, ExpandableGroup group) {
        final TitleMenu menu = (TitleMenu) group;

        /*if (selectedItemParent.equals(menu.getTitle()))
            holder.itemView.setBackground(ContextCompat.getDrawable(context, R.drawable.menu_selected_background));
        else
            holder.itemView.setBackgroundColor(Color.WHITE);*/

        holder.setGenreTitle(context, menu);

        if (menu.getItems().size() < 1) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    selectedItemParent = menu.getTitle();
                    selectedItemChild = "";
                    mListener.onMenuItemClick(menu.getTitle());
                    notifyDataSetChanged();
                }
            });
        }
    }

    public interface MenuItemClickListener {
        void onMenuItemClick(String itemString);
    }
}
