package com.txof.myapplication.ui.menu;

import android.view.View;
import android.widget.TextView;

import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;
import com.txof.myapplication.R;
import com.txof.myapplication.util.SharedUtils;


public class SubTitleViewHolder extends ChildViewHolder {
    private TextView subTitleTextView;
    private TextView countTextView;

    public SubTitleViewHolder(View itemView) {
        super(itemView);
        subTitleTextView = (TextView) itemView.findViewById(R.id.main_nav_submenu_item_title);
        countTextView = (TextView)itemView.findViewById(R.id.count);
    }

    public void setSubTitletName(String name) {
        subTitleTextView.setText(SharedUtils.toTitleCase(name));
    }

    public void setCount(int count){
        countTextView.setText(String.valueOf(count));
    }
}
