package com.txof.myapplication.ui.signup;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.txof.myapplication.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EnterpriseFragment extends Fragment {

    @BindView(R.id.lbl_empresa)
    EditText lblEmpresa;
    @BindView(R.id.lbl_nombre_gerente)
    EditText lblGerente;
    @BindView(R.id.lbl_celular)
    EditText lblCelular;
    @BindView(R.id.lbl_email)
    EditText lblEmail;

    public static EnterpriseFragment newInstance() {
        EnterpriseFragment fragment = new EnterpriseFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_signup_enterprise, container, false);
        ButterKnife.bind(this, view);
        return view;
    }
}
