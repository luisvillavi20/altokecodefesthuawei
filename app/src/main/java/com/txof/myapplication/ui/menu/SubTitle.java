package com.txof.myapplication.ui.menu;

import android.os.Parcel;
import android.os.Parcelable;

public class SubTitle implements Parcelable {
    private String name;
    private int count;

    public SubTitle(String name) {
        this.name = name;
    }

    public SubTitle(String name, int count) {
        this.name = name;
        this.count = count;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeInt(this.count);
    }

    protected SubTitle(Parcel in) {
        this.name = in.readString();
        this.count = in.readInt();
    }

    public static final Creator<SubTitle> CREATOR = new Creator<SubTitle>() {
        @Override
        public SubTitle createFromParcel(Parcel source) {
            return new SubTitle(source);
        }

        @Override
        public SubTitle[] newArray(int size) {
            return new SubTitle[size];
        }
    };
}
