package com.txof.myapplication.ui.activities;

import android.content.Intent;
import android.os.Bundle;

import com.cuneytayyildiz.onboarder.OnboarderActivity;
import com.cuneytayyildiz.onboarder.OnboarderPage;
import com.cuneytayyildiz.onboarder.utils.OnboarderPageChangeListener;
import com.txof.myapplication.R;
import com.txof.myapplication.ui.activities.LauncherActivity;
import com.txof.myapplication.util.SharedUtils;

import java.util.Arrays;
import java.util.List;

public class OnboardingActivity extends OnboarderActivity implements OnboarderPageChangeListener {

    List<OnboarderPage> pages;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pages = Arrays.asList(
                new OnboarderPage.Builder()
                        .title("Comunicación oportuna")
                        .description("Mantente conectado donde vayas y en cualquier dispositivo")
                        .imageResourceId(R.drawable.connect)
                        .backgroundColor(R.color.colorPrimaryDark)
                        .titleColor(R.color.slidesText)
                        .descriptionColor(R.color.slidesText)
                        .multilineDescriptionCentered(true)
                        .build(),

                new OnboarderPage.Builder()
                        .title("Accede estés donde estés")
                        .description("Recibe notificaciones oportunas estés donde estés")
                        .imageResourceId(R.drawable.city)
                        .backgroundColor(R.color.colorPrimary)
                        .titleColor(R.color.slidesText)
                        .descriptionColor(R.color.slidesText)
                        .multilineDescriptionCentered(true)
                        .build(),

                new OnboarderPage.Builder()
                        .title("Facil y sin complicaciones")
                        .description("Recibe y gestiona tus mensajes de forma fácil y ágil")
                        .imageResourceId(R.drawable.rocket)
                        .backgroundColor(R.color.colorPrimaryDark)
                        .titleColor(R.color.slidesText)
                        .descriptionColor(R.color.slidesText)
                        .multilineDescriptionCentered(true)
                        .build()
        );
        setOnboarderPageChangeListener(this);
        setSkipButtonTitle("Omitir");
        setFinishButtonTitle("Listo");
        initOnboardingPages(pages);
    }

    @Override
    public void onFinishButtonPressed() {
        startActivity(new Intent(this, LauncherActivity.class));
        finish();
        SharedUtils.setFirstRun(this, false);
    }

    @Override
    protected void onSkipButtonPressed() {
        startActivity(new Intent(this, LauncherActivity.class));
        finish();
        SharedUtils.setFirstRun(this, false);
    }

    @Override
    public void onPageChanged(int position) {
    }
}
