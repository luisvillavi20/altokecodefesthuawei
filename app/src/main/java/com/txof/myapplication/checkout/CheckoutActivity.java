package com.txof.myapplication.checkout;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.airbnb.lottie.LottieAnimationView;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.GsonBuilder;
import com.txof.myapplication.R;
import com.txof.myapplication.util.SharedUtils;
import com.txof.myapplication.vm.Cart;
import com.txof.myapplication.vm.OrderEntity;
import com.txof.myapplication.vm.OrderService;
import com.txof.myapplication.vm.ProductEntity;
import com.txof.myapplication.vm.ProductService;
import com.txof.myapplication.vm.UsuarioEntity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CheckoutActivity extends AppCompatActivity {
   public static final String ADDRESS_KEY = "address_key";
   private static final String TAG = "CHECKOUT_ACTIVITY";

   @BindView(R.id.txtDireccion)
   TextInputEditText txtDireccion;
   @BindView(R.id.completeAnimation)
   LottieAnimationView completeAnimation;
   @BindView(R.id.txtComplete)
   TextView txtComplete;
   @BindView(R.id.txtMonto)
   TextInputEditText txtMonto;
   @BindView(R.id.txtNotas)
   TextInputEditText txtNotas;

   @BindView(R.id.inputLayoutDireccion)
   TextInputLayout inputLayoutDireccion;
   @BindView(R.id.inputLayoutNotas)
   TextInputLayout inputLayoutNotas;
   @BindView(R.id.inputLayoutMonto)
   TextInputLayout inputLayoutMonto;
   @BindView(R.id.btnCompletar)
   Button btnCompletar;

   @Override
   protected void onCreate(@Nullable Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setTitle(getString(R.string.title_activity_checkout));
      setContentView(R.layout.activity_checkout);
      ButterKnife.bind(this);
      txtDireccion.setText(getIntent().getStringExtra(ADDRESS_KEY));
      txtMonto.setText(String.format("%.2f", Cart.getInstance().getTotal()));
   }

   @OnClick(R.id.btnCompletar)
   public void completar(View view) {
      Retrofit retrofit = new Retrofit.Builder()
              .baseUrl(this.getString(R.string.URL))
              .addConverterFactory(GsonConverterFactory.create())
              .build();
      OrderService orderService = retrofit.create(OrderService.class);
      Call<OrderEntity> call = orderService.order(
              SharedUtils.getUsuario(CheckoutActivity.this),
              SharedUtils.getToken(CheckoutActivity.this),
              txtNotas.getText().toString(),
              new GsonBuilder().create().toJson(Cart.getInstance().getProducts())
      );
      call.enqueue(new Callback<OrderEntity>() {
         @Override
         public void onResponse(Call<OrderEntity> call, Response<OrderEntity> response) {
            OrderEntity orderEntity = response.body();
            if (orderEntity.getERROR() == null) {
               completeAnimation.playAnimation();
               txtComplete.setVisibility(View.VISIBLE);

               inputLayoutDireccion.setVisibility(View.GONE);
               inputLayoutNotas.setVisibility(View.GONE);
               inputLayoutMonto.setVisibility(View.GONE);
               btnCompletar.setVisibility(View.GONE);
               Cart.getInstance().removeAll();
            } else {
               Log.e(TAG, orderEntity.getERROR());
            }

         }

         @Override
         public void onFailure(Call<OrderEntity> call, Throwable t) {

         }
      });
   }
}
