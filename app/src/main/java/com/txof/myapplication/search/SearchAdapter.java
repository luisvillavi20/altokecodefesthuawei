package com.txof.myapplication.search;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.txof.myapplication.R;
import com.txof.myapplication.vm.SearchEntity;

import java.util.ArrayList;
import java.util.List;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.ViewHolder> {
   protected List<SearchEntity> searchList = new ArrayList();
   protected Context context;

   public SearchAdapter(Context context) {
      this.context = context;
   }

   @NonNull
   @Override
   public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
      View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_item, parent, false);
      return new ViewHolder(view);
   }

   @Override
   public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
      SearchEntity searchEntity = searchList.get(position);
      holder.txtNombre.setText(searchEntity.getcNombre());
   }

   @Override
   public int getItemCount() {
      return this.searchList.size();
   }

   public void setData(List<SearchEntity> searchList) {
      this.searchList = searchList;
      notifyDataSetChanged();
   }

   public class ViewHolder extends RecyclerView.ViewHolder {
      private TextView txtNombre;
      public ViewHolder(@NonNull View itemView) {
         super(itemView);
         mxBind();
      }

      private void mxBind() {
         txtNombre = itemView.findViewById(R.id.txtNombre);
      }
   }
}
