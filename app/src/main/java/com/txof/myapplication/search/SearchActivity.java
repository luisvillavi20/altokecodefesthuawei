package com.txof.myapplication.search;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.txof.myapplication.R;
import com.txof.myapplication.vm.SearchEntity;
import com.txof.myapplication.vm.SearchService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SearchActivity extends AppCompatActivity {
   public static final String KEY_SEARCH_TEXT = "search_text_key";
   private RecyclerView recyclerViewSearch;
   private SearchAdapter searchAdapter;

   @Override
   protected void onCreate(@Nullable Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_search);
      getSupportActionBar().setDisplayHomeAsUpEnabled(true);
      getSupportActionBar().setTitle("Resultados de la busqueda");
      mxBind();
      mxSetup();
      mxLoad();
   }

   private void mxLoad() {
      Retrofit retrofit = new Retrofit.Builder()
              .baseUrl(getBaseContext().getString(R.string.URL))
              .addConverterFactory(GsonConverterFactory.create())
              .build();
      SearchService searchService = retrofit.create(SearchService.class);
      Call<List<SearchEntity>> call = searchService.getResult();
      call.enqueue(new Callback<List<SearchEntity>>() {
         @Override
         public void onResponse(Call<List<SearchEntity>> call, Response<List<SearchEntity>> response) {
            searchAdapter.setData(response.body());
         }

         @Override
         public void onFailure(Call<List<SearchEntity>> call, Throwable t) {
         }
      });
   }

   private void mxSetup() {
      searchAdapter = new SearchAdapter(this);
      recyclerViewSearch.setAdapter(searchAdapter);
      recyclerViewSearch.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
   }

   private void mxBind() {
      recyclerViewSearch = findViewById(R.id.recyclerViewSearch);
   }
}
